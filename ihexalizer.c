/* Look through an Intel hex file and figure out how many bytes of data
 * comprise the firmware. 
 *
 * Andrey Yurovsky <yurovsky@gmail.com> */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>

#define BYTE_COUNT_MAX  0xFF
#define ADDRESS_MAX     0xFFFF

static uint32_t ela;

static int get_start(const int fd)
{
    char c;
    if (read(fd, &c, sizeof(c)) != sizeof(c) || c != ':')
        return 1;

    return 0;
}

static int get_count(const int fd, uint16_t *count)
{
    char buf[sizeof(*count) + 1];

    if (read(fd, buf, sizeof(*count)) != sizeof(*count))
        return 1;

    buf[sizeof(*count)] = '\0';
    unsigned long val = strtoul(buf, NULL, 16);

    if (val > BYTE_COUNT_MAX)
        return 1;
    
    *count = (uint16_t)val;
    return 0;
}

static int get_address(const int fd, uint32_t *address)
{
    char buf[sizeof(*address) + 1];

    if (read(fd, buf, sizeof(*address)) != sizeof(*address))
        return 1;

    buf[sizeof(*address)] = '\0';
    unsigned long val = strtoul(buf, NULL, 16);

    if (val > ADDRESS_MAX)
        return 1;
    
    *address = (uint32_t)val;
    return 0;
}

static int get16(int fd, uint16_t *dest)
{
    char buf[3];

    if (read(fd, buf, 2) != 2)
        return 1;

    buf[2] = '\0';
    unsigned long val = strtoul(buf, NULL, 16);

    *dest = (uint16_t)val;
    return 0;
}

static int get_ela(const int fd)
{
    uint16_t a0, a1;

    if (!get16(fd, &a0) && !get16(fd, &a1)) {
        ela = ((uint32_t)a0 << 24) | ((uint32_t)a1 << 16);
        return 0;
    }

    return 1;
}

static int get_type(const int fd, char *type)
{
    char buf[2];

    if (read(fd, buf, 2) != 2 || buf[0] != '0')
        return 1;

    *type = buf[1];
    return 0;
}

static int analyze(const int fd)
{
    static unsigned long rec = 0;
    static unsigned long total = 0;

    if (get_start(fd)) {
        fprintf(stderr, "Error: expected start @ rec %u\n", rec);
        return -1;
    }

    uint16_t count;
    if (get_count(fd, &count)) {
        fprintf(stderr, "Error: unexpected byte count @ rec %u\n", rec);
        return -1;
    }

    uint32_t address;
    if (get_address(fd, &address)) {
        fprintf(stderr,
                "Error: unexpected address @ rec %u\n", rec);
        return -1;
    }

    char type;
    if (get_type(fd, &type)) {
        fprintf(stderr, "Error: unexpected record type @ rec %u\n", rec);
        return -1;
    }

    switch (type) {
        case '0':
            total += count;
            break;

        case '1':
            printf("%lu bytes\n", total);
            return 1;

        case '2':
            fputs("Warning: extended segment address", stderr);
            break;

        case '3':
            fputs("Warning: start segment address", stderr);
            break;

        case '4':
            if (get_ela(fd)) {
                fprintf(stderr, "Error: unable to parse ELA @ %u\n", rec);
                return -1;
            }
            off_t foo = lseek(fd, -4, SEEK_CUR);
            break;

        case '5':
            break;

        default:
            fprintf(stderr, "Error: unexpected record type @ rec %u\n", rec);
            return -1;
    }

    lseek(fd, count * 2 + 2, SEEK_CUR);

    /* Eat whitespace (newline) */
    char c;
    while (read(fd, &c, sizeof(c)) == sizeof(c)) {
        if (!isspace(c)) {
            lseek(fd, -1, SEEK_CUR);
            break;
        }
    }

    rec++;

    return 0;
}

int main(int argc, char **argv)
{
    if (argc < 2) {
        printf("Usage: %s </path/to/ihex>\n", argv[0]);
        exit(EXIT_SUCCESS);
    }

    char *path = realpath(argv[1], NULL);
    if (!path)
        exit(EXIT_FAILURE);
    
    int fd = open(path, O_RDONLY);
    free(path);

    if (fd == -1) {
        fprintf(stderr, "Unable to open \"%s\"\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    int res;
    do {
        res = analyze(fd);
    } while (res == 0);

    close(fd);

    return res == 1 ? EXIT_SUCCESS : EXIT_FAILURE;
}
